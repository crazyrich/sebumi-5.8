@extends('layouts.master')

@section('title', 'Sebumi | Our Stories')

@section('banner')

    <header class="set-header3">
        <div class="row set-margin flex-center">
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                <div class="set-faq">
                    <div class="putih font-36 bold tengah">The World of Stories</div>
                    <div class="putih tengah">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim Lorem </div>
                </div>
            </div>
        </div>
    </header>

@endsection

@section('content')

    <section id="portfolio" class="back-serviced section-padding ">
        <div class="container set-pd">
            <div class="tengah m-b-30">
                <div class="font-36 bold abu1">Highlight</div>
                <div class="bold">MOUNTAIN ECOTOURISM</div>
            </div>

            <div class="row set-margin">
                <div class="col-xl-8 col-lg-8 col-sm-12 col-sm-12 m-b-20">
                    <div class=" n-card">
                        <div class="row set-bl">
                            <div class="col-sm-12 putih bold">
                                <span ><img src="assets/image/icon/16.png" width="30px"></span></span>&nbsp;&nbsp;STORIES OF THE YEARS
                            </div>
                        </div>
                        <img src="assets/image/bahan/mount5.jpg" alt="..." class="img-thumbnail wt-us">
                        <div class="row set-bl-1">
                            <div class="col-sm-12 putih ">
                                <div>March 10, 2019</div>
                            </div>
                        </div>
                        <div class="row set-bl-2">
                            <div class="col-sm-12 font-28 putih bold ">
                                <div>Travel to Connect</div>
                            </div>
                        </div>
                        <div class="row set-bl-3">
                            <div class="col-sm-8 font-16 putih ">
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus porta ante eget metus feugiat luctus.</div>
                            </div>
                        </div>
                        <div class="row set-bl-4">
                            <div class="col-sm-12 font-16 putih ">
                                <div><a href="#" class="hejo">Read More</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-sm-6 col-sm-6 m-b-20">
                    <div class=" n-card m-b-20">
                        <div class="row set-blk">
                            <div class="col-sm-12 putih bold">
                                <span ><img src="assets/image/icon/17.png" width="30px"></span></span>&nbsp;&nbsp;FAVOURITES
                            </div>
                        </div>
                        <img src="assets/image/bahan/bromo2.jpg" alt="..." class="img-thumbnail wt-us">
                        <div class="row set-blk-1">
                            <div class="col-sm-12 putih ">
                                <div>March 10, 2019</div>
                            </div>
                        </div>
                        <div class="row set-blk-2">
                            <div class="col-sm-12 font-24 putih bold ">
                                <div>Travel to Connect</div>
                            </div>
                        </div>
                        <div class="row set-blk-4">
                            <div class="col-sm-12 font-16 putih ">
                                <div><a href="#" class="hejo">Read More</a></div>
                            </div>
                        </div>
                    </div>
                    <div class=" n-card ">
                        <div class="row set-blk">
                            <div class="col-sm-12 putih bold">
                                <!-- <span ><i class="fa fa-podcast fa-lg" aria-hidden="true"></i></span>&nbsp;&nbsp;NEW UPDATES -->
                                <span ><img src="assets/image/icon/18.png" width="30px"></span>&nbsp;&nbsp;NEW UPDATES
                            </div>
                        </div>
                        <img src="assets/image/bahan/bromo1.jpg" alt="..." class="img-thumbnail wt-us">
                        <div class="row set-blk-1">
                            <div class="col-sm-12 putih ">
                                <div>March 10, 2019</div>
                            </div>
                        </div>
                        <div class="row set-blk-2">
                            <div class="col-sm-12 font-24 putih bold ">
                                <div>Travel to Connect</div>
                            </div>
                        </div>
                        <div class="row set-blk-4">
                            <div class="col-sm-12 font-16 putih ">
                                <div><a href="#" class="hejo">Read More</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </section>

    <section id="portfolio" class="back-serviced section-padding-3 m-b-30">
        <div class="container set-pd">
            <div class="tengah m-b-50">
                <div class="font-36 bold abu1">Stories Collection</div>
                <div class="bold abu1">MOUNTAIN ECOTOURISM</div>
            </div>
            <div class="row set-margin">
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 m-b-20">
                    <div class="card ">
                        <img class="card-img-top" src="assets/image/bahan/mo1.PNG" alt="Card image cap">
                        <div class="card-body">
                            <div class="bold abu1 font-14 m-b-10">March 10, 2019</div>
                            <div class="font-18 abu1 bold m-b-10">
                                Lorem ipsum dolor sit amet, consectetuer Lorem ipsum
                            </div>
                            <div class="font-12 abu1 m-b-10">
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh sed diam nonummy nibh 
                            </div>
                            <div class="font-16 bold font-core">
                                Read more
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 m-b-20">
                        <div class="card ">
                            <img class="card-img-top" src="assets/image/bahan/mo2.PNG" alt="Card image cap">
                            <div class="card-body">
                                <div class="bold abu1 font-14 m-b-10">March 10, 2019</div>
                                <div class="font-18 abu1 bold m-b-10">
                                    Lorem ipsum dolor sit amet, consectetuer Lorem ipsum
                                </div>
                                <div class="font-12 abu1 m-b-10">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh sed diam nonummy nibh 
                                </div>
                                <div class="font-16 bold font-core">
                                    Read more
                                </div>
                            </div>
                        </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 m-b-20">
                    <div class="card ">
                        <img class="card-img-top" src="assets/image/bahan/mo3.PNG" alt="Card image cap">
                        <div class="card-body">
                            <div class="bold abu1 font-14 m-b-10">March 10, 2019</div>
                            <div class="font-18 abu1 bold m-b-10">
                                Lorem ipsum dolor sit amet, consectetuer Lorem ipsum
                            </div>
                            <div class="font-12 abu1 m-b-10">
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh sed diam nonummy nibh 
                            </div>
                            <div class="font-16 bold font-core">
                                Read more
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 m-b-20">
                        <div class="card ">
                            <img class="card-img-top" src="assets/image/bahan/mo4.PNG" alt="Card image cap">
                            <div class="card-body">
                                <div class="bold abu1 font-14 m-b-10">March 10, 2019</div>
                                <div class="font-18 abu1 bold m-b-10">
                                    Lorem ipsum dolor sit amet, consectetuer Lorem ipsum
                                </div>
                                <div class="font-12 abu1 m-b-10">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh sed diam nonummy nibh 
                                </div>
                                <div class="font-16 bold font-core">
                                    Read more
                                </div>
                            </div>
                        </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 m-b-20">
                    <div class="card ">
                        <img class="card-img-top" src="assets/image/bahan/mo1.PNG" alt="Card image cap">
                        <div class="card-body">
                            <div class="bold abu1 font-14 m-b-10">March 10, 2019</div>
                            <div class="font-18 abu1 bold m-b-10">
                                Lorem ipsum dolor sit amet, consectetuer Lorem ipsum
                            </div>
                            <div class="font-12 abu1 m-b-10">
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh sed diam nonummy nibh 
                            </div>
                            <div class="font-16 bold font-core">
                                Read more
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 m-b-20">
                        <div class="card ">
                            <img class="card-img-top" src="assets/image/bahan/mo2.PNG" alt="Card image cap">
                            <div class="card-body">
                                <div class="bold abu1 font-14 m-b-10">March 10, 2019</div>
                                <div class="font-18 abu1 bold m-b-10">
                                    Lorem ipsum dolor sit amet, consectetuer Lorem ipsum
                                </div>
                                <div class="font-12 abu1 m-b-10">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh sed diam nonummy nibh 
                                </div>
                                <div class="font-16 bold font-core">
                                    Read more
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </section>

    <section id="service" class="section-padding-2 ">
        <div class="container set-pd pd-20 set-bg1">
            <div class="tengah m-b-50 putih">
                <div class="font-36 bold ">Share your Story!</div>
                <div class="bold">SUBMIT STORIES</div>
            </div>
            
            <div class="row set-margin flex-center m-b-30">
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                    <div class="row">
                        <div class="col-sm-2 m-b-10">
                            <div class="tengah" >
                                <div class="putih"><i class="fa fa-smile-o fa-3x" aria-hidden="true"></i></div>
                                <div class="putih">Feel</div>
                            </div>
                        </div>
                        <div class="col-sm-2 m-b-10">
                            <div class="tengah " >
                                <div class="putih m-t-10"><i class="fa fa-arrow-right fa-lg" aria-hidden="true"></i></div>
                            </div>
                        </div>
                        <div class="col-sm-2 m-b-10">
                            <div class="tengah" >
                                <div class="putih"><i class="fa fa-pencil fa-3x" aria-hidden="true"></i></div>
                                <div class="putih">Write</div>
                            </div>
                        </div>
                        <div class="col-sm-2 m-b-10">
                            <div class="tengah " >
                                <div class="putih m-t-10"><i class="fa fa-arrow-right fa-lg" aria-hidden="true"></i></div>
                            </div>
                        </div>
                        <div class="col-sm-2 m-b-10">
                            <div class="tengah " >
                                <div class="putih"><i class="fa fa-paper-plane-o fa-3x" aria-hidden="true"></i></div>
                                <div class="putih">Share!</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row set-margin flex-center m-b-30">
                <div class="col-sm-8 putih font-20">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea 
                </div>
            </div>

            
            <div class="row set-margin flex-center">
                <div class="col-md-4">
                    <button class="btn-search font-18 m-b-20">Submit</button>
                </div>
            </div>
            
        </div>
    </section>

@endsection