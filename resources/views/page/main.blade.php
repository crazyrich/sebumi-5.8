@extends('layouts.master')

@section('title', 'Sebumi | Main')

@section('banner')
    
    <header class="set-header h-80-mobile">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators bottom1" >
              <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner h-80-mobile">
              <div class="carousel-item active">
                <img class="d-block w-100 h-80-mobile" src="assets/image/bahan/couple-travel.jpg" alt="First slide">
                <div class="carousel-caption  d-md-block kiri text-nav">
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="bold font-70 l-h-75 m-b-15">
                                Our <br>
                                Packages
                            </div>
                            <div class="m-b-20">
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim .Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed 
                            </div>
                            <div>
                                <button class="btn-see">See Stories</button>
                            </div>
                        </div>
                    </div>
                </div>
              </div>

              <div class="carousel-item">
                <img class="d-block w-100 h-80-mobile" src="assets/image/bahan/4.jpg" alt="First slide">
                <div class="carousel-caption  d-md-block kiri text-nav">
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="bold font-70 l-h-75 m-b-15">
                                Our <br>
                                Packages
                            </div>
                            <div class="m-b-20">
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim .Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed 
                            </div>
                            <div>
                                <button class="btn-see">See Stories</button>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="carousel-item">
                <img class="d-block w-100 h-80-mobile" src="assets/image/bahan/5.jpg" alt="First slide">
                <div class="carousel-caption  d-md-block kiri text-nav">
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="bold font-70 l-h-75 m-b-15">
                                Our <br>
                                Packages
                            </div>
                            <div class="m-b-20">
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim .Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed 
                            </div>
                            <div>
                                <button class="btn-see">See Stories</button>
                            </div>
                        </div>
                    </div>
                </div>
              </div>

            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev" >
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
        </div>

    </header>

@endsection

@section('content')
    
    <section id="about">
        <div class="container get-pesan">
            <div class="row set-margin">
                <div class="col-sm-3">
                    <select class="form-control">
                        <option>Select Trip</option>
                        <option>Select Trip</option>
                        <option>Select Trip</option>
                    </select>
                </div>
                <div class="col-sm-3">
                    <select class="form-control">
                        <option>Select Categories</option>
                        <option>Select Trip</option>
                        <option>Select Trip</option>
                    </select>
                </div>
                <div class="col-sm-3">
                    <select class="form-control">
                        <option>Any Month</option>
                        <option>Select Trip</option>
                        <option>Select Trip</option>
                    </select>
                </div>
                <div class="col-sm-3">
                    <button class="btn-search">Search</button>
                </div>
            </div>

        </div>
    </section>

    <section id="skill" class="back-skill ">
        <div class="container  set-pd">
            <div class="row pd-50-0 m-t-100 set-margin">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 tengah">
                        <iframe class="respon-embed" src="https://www.youtube.com/embed/a198K0u1_uw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <!-- <img src="assets/image/bahan/couple.jpg" class="img-fluid" alt="Responsive image"> -->
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                    <div class="pd-20">
                        <div class="font-36 bold abu1">Travel to Connect</div>
                    </div>
                    <div class="pd-20">
                        <div>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus porta ante eget metus feugiat luctus. Cras tincidunt bibendum lectus, a bibendum quam pharetra non. Morbi consequat quam turpis, et maximus erat malesuada sit amet. Nullam odio lectus, vestibulum eget erat non,
                        </div>
                    </div>
                    <div class="pd-20">
                        <button class="btn-see">See More</button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="service" class=" section-padding ">
        <div class="container  set-pd">
            <div class="tengah m-b-50">
                <div class="font-36 bold abu1">Travel with Us</div>
                <div class="bold">OUR PACKAGES CATEGORIES</div>
            </div>


            <div id="carouselExampleIndicators1" class="carousel slide" data-ride="carousel" style="    height: 600px;">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active" style="height:600px">
                        <div class="carousel-caption  d-md-block kiri text-nav2" style="height:600px; left: 0%;">
                            <div class="row m-b-50 set-margin">
                                <div class="col-sm-4 m-b-20">
                                    <div class="card n-card tes1">
                                            
                                        <div class="overlay">
                                            <div class="m-b-20 pd-20 mt-10s">
                                                <div class="font-16 text-center">
                                                    <img src="assets/image/icon/6.png" width="70px">
                                                </div>
                                                <div class="font-14 text-center m-b-10">
                                                    <div class="font-26" style="line-height: 33px;"><b>Sustainability Workshop</b></div>
                                                </div>
                                                <div class="font-12 text-center">2 TOURS</div>
                                            </div>
                                        </div>
        
                                        <img src="assets/image/bahan/with1.PNG" alt="..." class="img-thumbnail wt-us">
                                    </div>
                                </div>
                                <div class="col-sm-4 m-b-20">
                                    <div class="card n-card tes1">
                                        <div class="overlay">
                                            <div class="m-b-20 pd-20 mt-10s">
                                                <div class="font-16 text-center">
                                                    <img src="assets/image/icon/7.png" width="70px">
                                                </div>
                                                <div class="font-14 text-center m-b-10">
                                                    <div class="font-26" style="line-height: 33px;"><b>Signature Trip</b></div>
                                                </div>
                                                <div class="font-12 text-center">3 TOURS</div>
                                            </div>
                                        </div>
        
                                        <img src="assets/image/bahan/with2.PNG" alt="..." class="img-thumbnail wt-us">
                                    </div>
                                </div>
                                <div class="col-sm-4 m-b-20">
                                    <div class="card n-card tes1">
                                        <div class="overlay">
                                            <div class="m-b-20 pd-20 mt-10s">
                                                <div class="font-16 text-center">
                                                    <img src="assets/image/icon/8.png" width="70px">
                                                </div>
                                                <div class="font-14 text-center m-b-10">
                                                    <div class="font-26" style="line-height: 33px;"><b>Custom Trip</b></div>
                                                </div>
                                                <div class="font-12 text-center">2 TOURS</div>
                                            </div>
                                        </div>
                                        <img src="assets/image/bahan/with3.PNG" alt="..." class="img-thumbnail wt-us">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item" style="height:600px">
                        <div class="carousel-caption  d-md-block kiri text-nav2" style="height:600px; left: 0%;">
                            <div class="row m-b-50 set-margin">
                                <div class="col-sm-4 m-b-20">
                                    <div class="card n-card tes1">
                                            
                                        <div class="overlay">
                                            <div class="m-b-20 pd-20 mt-10s">
                                                <div class="font-16 text-center">
                                                    <img src="assets/image/icon/6.png" width="70px">
                                                </div>
                                                <div class="font-14 text-center m-b-10">
                                                    <div class="font-26" style="line-height: 33px;"><b>Sustainability Workshop</b></div>
                                                </div>
                                                <div class="font-12 text-center">2 TOURS</div>
                                            </div>
                                        </div>
        
                                        <img src="assets/image/bahan/with1.PNG" alt="..." class="img-thumbnail wt-us">
                                    </div>
                                </div>
                                <div class="col-sm-4 m-b-20">
                                    <div class="card n-card tes1">
                                        <div class="overlay">
                                            <div class="m-b-20 pd-20 mt-10s">
                                                <div class="font-16 text-center">
                                                    <img src="assets/image/icon/7.png" width="70px">
                                                </div>
                                                <div class="font-14 text-center m-b-10">
                                                    <div class="font-26" style="line-height: 33px;"><b>Signature Trip</b></div>
                                                </div>
                                                <div class="font-12 text-center">3 TOURS</div>
                                            </div>
                                        </div>
        
                                        <img src="assets/image/bahan/with2.PNG" alt="..." class="img-thumbnail wt-us">
                                    </div>
                                </div>
                                <div class="col-sm-4 m-b-20">
                                    <div class="card n-card tes1">
                                        <div class="overlay">
                                            <div class="m-b-20 pd-20 mt-10s">
                                                <div class="font-16 text-center">
                                                    <img src="assets/image/icon/8.png" width="70px">
                                                </div>
                                                <div class="font-14 text-center m-b-10">
                                                    <div class="font-26" style="line-height: 33px;"><b>Custom Trip</b></div>
                                                </div>
                                                <div class="font-12 text-center">2 TOURS</div>
                                            </div>
                                        </div>
                                        <img src="assets/image/bahan/with3.PNG" alt="..." class="img-thumbnail wt-us">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators1" role="button" data-slide="prev" style="left: -3rem;">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators1" role="button" data-slide="next" style="right: -3rem;">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

            <!-- <div class="row m-b-50 set-margin">
                <div class="col-sm-4 m-b-20">
                    <div class="card n-card">
                        <img src="assets/image/bahan/with1.PNG" alt="..." class="img-thumbnail wt-us">
                    </div>
                </div>
                <div class="col-sm-4 m-b-20">
                    <div class="card n-card">
                        <img src="assets/image/bahan/with2.PNG" alt="..." class="img-thumbnail wt-us">
                    </div>
                </div>
                <div class="col-sm-4 m-b-20">
                    <div class="card n-card">
                        <img src="assets/image/bahan/with3.PNG" alt="..." class="img-thumbnail wt-us">
                    </div>
                </div>
            </div> -->

        </div>
    </section>

    <section id="testimony" class="section-padding set-testi hire-me text-center cover-bg ">
        <div class="container set-pd">
            <div class="tengah  m-b-30">
                <div class="font-36 bold font-core">Schdules</div>
                <div class="bold putih">OUR YEARLY SCHDULES</div>
            </div>
            
            <div class="map-responsive">
                <div id="map"></div>
            </div>
        </div>
    </section>
    
    <section id="portfolio" class="back-serviced section-padding-3 ">
        <div class="container set-pd m-t-100">
            <div class="tengah m-b-30">
                <div class="font-36 bold abu1">Stories</div>
                <div class="bold">OUR PACKAGES CATEGORIES</div>
            </div>

            <div class="row set-margin m-b-100">
                <div class="col-xl-8 col-lg-8 col-sm-12 col-sm-12 m-b-20">
                    <div class=" n-card">
                        <div class="row set-bl">
                            <div class="col-sm-12 putih bold">
                                <span ><i class="fa fa-star fa-lg" aria-hidden="true"></i></span>&nbsp;&nbsp;STORIES OF THE YEARS
                            </div>
                        </div>
                        <img src="assets/image/bahan/mount5.jpg" alt="..." class="img-thumbnail wt-us">
                        <div class="row set-bl-1">
                            <div class="col-sm-12 putih ">
                                <div>March 10, 2019</div>
                            </div>
                        </div>
                        <div class="row set-bl-2">
                            <div class="col-sm-12 font-28 putih bold ">
                                <div>Travel to Connect</div>
                            </div>
                        </div>
                        <div class="row set-bl-3">
                            <div class="col-sm-8 font-16 putih ">
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus porta ante eget metus feugiat luctus.</div>
                            </div>
                        </div>
                        <div class="row set-bl-4">
                            <div class="col-sm-12 font-16 putih ">
                                <div><a href="#" class="hejo">Read More</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-sm-6 col-sm-6 m-b-20">
                    <div class=" n-card m-b-20">
                        <div class="row set-blk">
                            <div class="col-sm-12 putih bold">
                                <span ><i class="fa fa-heart fa-lg" aria-hidden="true"></i></span>&nbsp;&nbsp;FAVOURITES
                            </div>
                        </div>
                        <img src="assets/image/bahan/bromo2.jpg" alt="..." class="img-thumbnail wt-us">
                        <div class="row set-blk-1">
                            <div class="col-sm-12 putih ">
                                <div>March 10, 2019</div>
                            </div>
                        </div>
                        <div class="row set-blk-2">
                            <div class="col-sm-12 font-24 putih bold ">
                                <div>Travel to Connect</div>
                            </div>
                        </div>
                        <div class="row set-blk-4">
                            <div class="col-sm-12 font-16 putih ">
                                <div><a href="#" class="hejo">Read More</a></div>
                            </div>
                        </div>
                    </div>
                    <div class=" n-card ">
                        <div class="row set-blk">
                            <div class="col-sm-12 putih bold">
                                <!-- <span ><i class="fa fa-podcast fa-lg" aria-hidden="true"></i></span>&nbsp;&nbsp;NEW UPDATES -->
                                <span ><img src="assets/image/icon/18.png" width="30px"></span>&nbsp;NEW UPDATES
                            </div>
                        </div>
                        <img src="assets/image/bahan/bromo1.jpg" alt="..." class="img-thumbnail wt-us">
                        <div class="row set-blk-1">
                            <div class="col-sm-12 putih ">
                                <div>March 10, 2019</div>
                            </div>
                        </div>
                        <div class="row set-blk-2">
                            <div class="col-sm-12 font-24 putih bold ">
                                <div>Travel to Connect</div>
                            </div>
                        </div>
                        <div class="row set-blk-4">
                            <div class="col-sm-12 font-16 putih ">
                                <div><a href="#" class="hejo">Read More</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection