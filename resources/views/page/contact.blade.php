@extends('layouts.master')

@section('title', 'Sebumi | Contact Us')

@section('banner')

    <header class="set-header4">
        <div class="row set-margin flex-center">
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                <div class="set-faq">
                    <div class="putih font-36 bold tengah">Contact Us</div>
                    <div class="putih tengah">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim Lorem </div>
                </div>
            </div>
        </div>
    </header>

@endsection

@section('content')

    <section id="skill" class="section-padding-3">
        <div class="container  set-pd">
            <div class="row set-margin">
                <div class="col-sm-4">
                    <div class="card-body">
                        <div class="bold font-36 m-b-50 l-h-35">Contact Information</div>
                        <div class="font-16 m-b-20">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, </div>
                        <div class="font-16 m-b-20">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, </div>
                        <div class="font-16 m-b-20">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, </div>
                        <div class="font-16 m-b-20">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="card-body">
                        <div class="row m-b-20">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="bold">Name*</label>
                                    <input type="text" class="form-control" placeholder="First Name">
                                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="bold">Last Name</label>
                                    <input type="text" class="form-control" placeholder="Last Name">
                                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                                </div>
                            </div>
                        </div>
                        <div class="row m-b-20">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="bold">Email Adress*</label>
                                    <input type="text" class="form-control" placeholder="Email Address">
                                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                                </div>
                            </div>
                        </div>
                        <div class="row m-b-20">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="bold">Message*</label>
                                    <textarea type="text" class="form-control" rows="5" placeholder="Message"></textarea>
                                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                                </div>
                            </div>
                        </div>
                        <div class="row m-b-20">
                            <div class="col-sm-4">
                                <button class="btn-search">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection