<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Favicon -->
    <link rel="icon" href="assets/image/logo3.png" width="25px" height="25px" type="image/x-icon">
    
    <!-- Stylesheet CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/sebumi.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <script type="text/javascript">
		$(window).on('scroll', function() {
			if($(window).scrollTop()){
				$('nav').addClass('hitam');
			}else{
				$('nav').removeClass('hitam');
			}
		})
		
		// When the user scrolls down 20px from the top of the document, show the button
		window.onscroll = function() {scrollFunction()};

		function scrollFunction() {
			if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
				document.getElementById("myBtn").style.display = "block";
			} else {
				document.getElementById("myBtn").style.display = "none";
			}
		}

		// When the user clicks on the button, scroll to the top of the document
		function topFunction() {
			document.body.scrollTop = 0;
			document.documentElement.scrollTop = 0;
		}

        $(window).on("load",function() {
            $(window).scroll(function() {
                var windowBottom = $(this).scrollTop() + $(this).innerHeight();
                $(".fade").each(function() {
                /* Check the location of each desired element */
                var objectBottom = $(this).offset().top + $(this).outerHeight();
                
                /* If the element is completely within bounds of the window, fade it in */
                if (objectBottom < windowBottom) { //object comes into view (scrolling down)
                    if ($(this).css("opacity")==0) {$(this).fadeTo(500,1);}
                } else { //object goes out of view (scrolling up)
                    if ($(this).css("opacity")==1) {$(this).fadeTo(500,0);}
                }
                });
            }).scroll(); //invoke scroll-handler on page-load
        });

	</script>

	<style>
		html {
          scroll-behavior: smooth;
          transition: 2s;
		}
		.hitam {
			background:rgba(0, 0, 0, 0.7)!important;
        }

        #map {
            height: 100vh;
        }
        
        .gm-style .gm-style-iw-c {
            border-radius: 5px !important;
            padding: 0px !important;
        }
        .gm-style-iw-d {
            overflow: auto !important; 
            overflow-x: hidden !important; 
        }

	</style>

    @yield('heading')
	
  </head>
  
  <body>
	<button onclick="topFunction()" id="myBtn" title="Go to top"><i class="fa  fa-lg fa-arrow-up" aria-hidden="true"></i></button>
	<div id="topmarkotop">
        <nav class="navbar navbar-expand-lg">
            <div class="container  set-pd">
                <a class="navbar-brand white" href="/" style="margin-left:15px;">
                    <!-- <img src="assets/image/bahan/logo-s.png" height="30px" width="30px" class="rounded" style="margin-top: -5px;"> -->
                    <img src="assets/image/logo_putih1.png"  class="rounded" style="margin-top: -10px; width: 110px;">
                    <!-- <span class="font-24 bold">sebumi</span>  -->
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon putih"><i class="fa fa-bars" aria-hidden="true"></i></span>
                </button>

                <div class="collapse navbar-collapse flex-right " id="navbarSupportedContent" style="padding: 0px 10px;">
                    <ul class="navbar-nav font-16">                        
                        <li class="nav-item">
                            <a class="nav-link white bold" href="#about">About Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link white bold " href="/packages">Packages</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link white bold " href="/stories">Our Stories</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link white bold" href="#portfolio">Sebumi Berbagi</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link white bold" href="/contact">Contact Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link white bold" href="#testimony">Log In</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
	</div>

    @yield('banner')

    @yield('content')

    <footer class="footer">
        <div class="container set-pd">
            <div class="row m-b-30 set-margin">
                <div class="col-sm-3">
                    <div class="m-b-5">
                        <!-- <img src="assets/image/bahan/logo-s.png" height="25px" width="25px" class="rounded" style="margin-top: -5px;">
                        <span class="font-24 bold putih">sebumi</span>  -->
                        <img src="assets/image/logo_putih1.png"  class="rounded" style="margin-top: -5px; width: 110px;">
                    </div>
                    <div class="putih font-14 m-b-30">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus porta ante eget metus feugiat luctus.
                    </div>
                    <div>
                        <span class="putih set-tw"><i class="fa fa-youtube-play fa-lg" aria-hidden="true"></i></span> 
                        <span class="putih set-fb"><i class="fa fa-facebook fa-lg" aria-hidden="true"></i></span> 
                        <span class="putih set-ig"><i class="fa fa-instagram fa-lg" aria-hidden="true"></i></span> 
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="m-b-15">
                        <span class="font-14 bold font-core">Contact Info</span> 
                    </div>
                    <div class="putih font-14 m-b-30">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus porta ante eget metus feugiat luctus.
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="m-b-15">
                        <span class="font-14 bold font-core">Career</span> 
                    </div>
                    <div class="putih font-14 m-b-30">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus porta ante eget metus feugiat luctus.
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="m-b-15">
                        <span class="font-14 bold font-core">Newsletter</span> 
                    </div>
                    <div class="putih font-14 m-b-30">
                        <input type="text" class="m-b-10 set-form" placeholder="Your Email Address">
                        <button class="btn-search">Subcribe</button>
                    </div>
                </div>
            </div>

            <div class="row set-margin" >
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12 m-tengah">
                        <span class="putih font-12">&copy; Copyright Sebumi Berbagi</span>
                </div>
                <div id="footer-bawah" class="col-xl-9 col-lg-9 col-md-8 col-sm-12">
                    <div class="kanan font-12 putih">
                        <ul id="menu-bawah">
                            <li class="m-l-20 cursor">Home</li>
                            <li class="m-l-20 cursor">About Us</li>
                            <li class="m-l-20 cursor">Packages</li>
                            <li class="m-l-20 cursor">Blogs</li>
                            <li class="m-l-20 cursor">Sebumi Berbagi</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ asset('/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('/js/popper.min.js') }}"></script>
    <script src="{{ asset('/js/jquery.bubble.text.js') }}"></script>
    <script src="{{ asset('/js/script.js') }}"></script>
    <script src="{{ asset('/js/bootstrap.min.js') }}"></script>

  </body>
</html>