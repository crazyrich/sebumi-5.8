<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function masterView(){
        return view('layouts.master');
    }

    public function mainView(){
        return view('page.main');
    }
}
