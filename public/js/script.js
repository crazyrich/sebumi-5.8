$(document).ready(function() {

    var $element = $('#bubble');
    var newText = 'IM PROGRAMMER';

    bubbleText({
        element: $element,
        newText: newText,
        speed: 3000,
        repeat: Infinity,
        result: newText.fontsize(40)
    });

});
