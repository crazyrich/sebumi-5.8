<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SiteController@mainView');
Route::get('/packages', 'PackagesController@index');
Route::get('/stories', 'StoriesController@index');
Route::get('/contact', 'ContactController@index');
